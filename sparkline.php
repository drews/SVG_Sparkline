<?php
/**
 * Created by danieldrews
 *
 * Constructing a simple SVG line graph.
 * Use setData() with an array of values and they will be evenly distributed along the x-axis.
 * Values could be int or float. Or 'null' for empty values.
 */

namespace svg;

class sparkline
{
    private $svgW = 300;
    private $svgH = 70;
    private $maxX = 1000;
    private $maxY = 200;

    private $data;
    private $graphData;

    private $strokeWidth = 4;
    private $strokeColor = 'black';

    private $showPoints = true;
    private $pointRadius = 8;
    private $pointColor = 'black';

    private $showZeroLine = true;
    private $zeroLineWidth = 2;
    private $zeroLineColor = 'green';

    private $zeroAsBase = false;

    private $showAverage = true;
    private $averageWidth = 2;
    private $averageColor = 'orange';

    private $darkStrokeColor = 'white';
    private $darkPointColor = 'white';
    private $darkZeroLineColor = 'lightgreen';
    private $darkAverageColor = 'yellow';


    public function __construct(){

    }

    public function setData($data = false){
        if(!$data){
            $this->data = $this->getRandomData();
        }else{
            $this->data = $data;
        }
    }

    private function getRandomData(){
        $data = array();
        //$data[] = -100;
        for ($i = 0; $i <= rand(1,50); $i++){
            $data[] = rand(-1000,2000);
        }
        return $data;
    }

    public function getSVG(){
        if(!$this->data) return 'Keine Daten!';
        $this->graphData = array();
        $start = true;
        $interval = $this->maxX / (sizeof($this->data)-1);
        $maxY = $this->maxY;

        $maxValue = max($this->data);
        $minValue = min($this->data);
        if($minValue > 0 && $this->zeroAsBase) $minValue = 0;

        $unit = $maxY / ($maxValue-$minValue);
        $yOffset = $minValue*$unit;

        $pathData = '';
        foreach ($this->data as $key => $value){
            if(is_null($value)) continue;
            $x = ($key)*$interval;
            $y = $maxY-($value*$unit)+$yOffset;
            $this->graphData[$x] = $y;
            if($start){
                $pathData .= 'M'.$x.','.$y.' ';
                $start = false;
            }else{
                $pathData .= 'L'.$x.','.$y.' ';
            }
        }

        //Fläche erweitern, sonst werden die Punkte angeschnitten
        if($this->showPoints){
            $viewBoxStart = '-'.$this->pointRadius;
            $viewBoxWidth = $this->maxX+($this->pointRadius*2);
        }else{
            $viewBoxStart = 0;
            $viewBoxWidth = $this->maxX;
        }

        $htmlString = '<svg width="'.$this->svgW.'" height="'.$this->svgH.'" viewBox="'.$viewBoxStart.' 0 '.$viewBoxWidth.' '.$this->maxY.'" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">';

        if($this->showZeroLine){
            if($minValue < 0){
                $zero = $maxY-(0*$unit)+$yOffset;
                $zeroLine = '<path d="M '.$viewBoxStart.','.$zero.' L '.$viewBoxWidth.','.$zero.'" stroke="'.$this->zeroLineColor.'" stroke-width="'.$this->zeroLineWidth.'" fill="none" />';
            }else{$zeroLine = '';}
            $htmlString .= $zeroLine;
        }

        if($this->showAverage){
            $valuesCombined = 0;
            foreach ($this->data as $value){
                $valuesCombined += $value;
            }
            $averageValue = $valuesCombined/sizeof($this->data);
            $average = $maxY-($averageValue*$unit)+$yOffset;
            $averageLine = '<path d="M '.$viewBoxStart.','.$average.' L '.$viewBoxWidth.','.$average.'" stroke="'.$this->averageColor.'" stroke-width="'.$this->averageWidth.'" fill="none" />';
            $htmlString .= $averageLine;
        }

        $htmlString .= '<path d="'.$pathData.'" style="stroke:'.$this->strokeColor.'; stroke-width:'.$this->strokeWidth.'; fill:none;"/>';

        if($this->showPoints){
            $pointData = '<g>';
            foreach ($this->graphData as $key => $value){
                $pointData .= '<circle cx="'.$key.'" cy="'.$value.'" r="'.$this->pointRadius.'" fill="'.$this->pointColor.'"></circle>';
            }
            $pointData .= '</g>';
            $htmlString .= $pointData;
        }

        $htmlString .='</svg>';

        return $htmlString;
    }

    public function setSizeMultiplier($multiplier)
    {
        $this->svgH = $this->svgH*$multiplier;
        $this->svgW = $this->svgW*$multiplier;
    }

    public function setSvgH($svgH)
    {
        $this->svgH = $svgH;
    }

    public function setSvgW($svgW)
    {
        $this->svgW = $svgW;
    }

    public function useDarkTheme(){
        $this->strokeColor = $this->darkStrokeColor;
        $this->pointColor = $this->darkPointColor;
        $this->zeroLineColor = $this->darkZeroLineColor;
        $this->averageColor = $this->darkAverageColor;
    }

    public function setStrokeWidth($strokeWidth)
    {
        $this->strokeWidth = $strokeWidth;
    }

    public function setStrokeColor($strokeColor)
    {
        $this->strokeColor = $strokeColor;
    }

    public function showPoints($showPoints)
    {
        $this->showPoints = $showPoints;
    }

    public function setPointRadius($pointRadius)
    {
        $this->pointRadius = $pointRadius;
    }

    public function setPointColor($pointColor)
    {
        $this->pointColor = $pointColor;
    }

    public function showZeroLine($showZeroLine)
    {
        $this->showZeroLine = $showZeroLine;
    }

    public function setZeroLineColor($zeroLineColor)
    {
        $this->zeroLineColor = $zeroLineColor;
    }

    public function setZeroLineWidth($zeroLineWidth)
    {
        $this->zeroLineWidth = $zeroLineWidth;
    }

    public function setZeroAsBase($zeroAsBase)
    {
        $this->zeroAsBase = $zeroAsBase;
    }

    public function showAverage($showAverage)
    {
        $this->showAverage = $showAverage;
    }

    public function setAverageColor($averageColor)
    {
        $this->averageColor = $averageColor;
    }

    public function setAverageWidth($averageWidth)
    {
        $this->averageWidth = $averageWidth;
    }

    public function getSvgW()
    {
        return $this->svgW;
    }

    public function getSvgH()
    {
        return $this->svgH;
    }
}
